-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 24, 2019 at 04:01 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `k_means`
--
CREATE DATABASE IF NOT EXISTS `k_means` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `k_means`;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `nota` int(11) NOT NULL,
  `id_pel` varchar(5) NOT NULL,
  `total_pembelian` char(255) NOT NULL,
  `tgl_beli` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`nota`, `id_pel`, `total_pembelian`, `tgl_beli`) VALUES
(1, 'A', '450000', '2019-02-02'),
(2, 'B', '4500000', '2019-06-09'),
(3, 'C', '120000', '2019-08-04'),
(4, 'D', '315000', '2019-07-05'),
(5, 'A', '50000', '2019-01-06'),
(6, 'A', '650000', '2019-03-07'),
(7, 'B', '1230000', '2019-04-08'),
(8, 'C', '750000', '2019-06-09'),
(9, 'C', '565000', '2019-07-10'),
(10, 'D', '5700000', '2019-06-11'),
(11, 'A', '90000', '2019-03-20'),
(12, 'B', '250000', '2019-06-25'),
(13, 'E', '150000', '2019-07-02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`nota`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `nota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
