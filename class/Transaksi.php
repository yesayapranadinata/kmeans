<?php 
if(!class_exists('database')){
	require('Database.php');
}

class Transaksi{
	public $nota;
	public $id_pel;
	public $total_pembelian;
	public $tgl_beli;



	public function getData(){
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "select id_pel, SUM(total_pembelian) as Total_Transaksi, COUNT(nota)as Jumlah_Transaksi, datediff(current_date(), (MAX(tgl_beli))) as selisih from transaksi GROUP BY id_pel";
		$data = $dbConnect->query($sql);
		$dbConnect = $db->close();
		return $data;
	}	

	public function getMinJumlahTransaksi(){
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "SELECT MIN(Jumlah_Transaksi), MAX(Jumlah_Transaksi), MIN(selisih), MAX(selisih),MIN(Total_Transaksi),MAX(Total_Transaksi) FROM (select id_pel, SUM(total_pembelian) as Total_Transaksi, COUNT(nota)as Jumlah_Transaksi, datediff(current_date(), (MAX(tgl_beli))) as selisih from transaksi GROUP BY id_pel) as hasil";
		$data = $dbConnect->query($sql);
		$dbConnect = $db->close();
		return $data;
	}
	
}

?>
